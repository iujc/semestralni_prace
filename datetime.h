typedef struct {
    int year;
    int month;
    int day;
    int hour;
    int min;
    int sec;
    int dayInMonth;
} tDateTime;
//a deklaraci funkcí:
tDateTime dejDateTime(char* aDatetime);
// převádí vstup (2018-05-01 01:00:00) na strukturu, využívá dejDenVTydnu
int dejDenVTydnu(int aR, int aM, int aD);
//vrací den v týdnu (0-PO,…,6-NE) *výpočet viz poznámka