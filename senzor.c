#include "senzor.h"
#include <stdio.h>
/*typedef struct senzor{
    int id;
    tDateTime timestamp;
    float m3;
    struct senzor* dalsi;
}tSenzor;*/

// vrací adresu dynamické alokace senzoru, ukazatel dalsi se nastaví na NULL
tSenzor * vytvorSenzor(int id, tDateTime ts,float m3){
    tSenzor s;
    s.id = id;
    s.timestamp = ts;
    s.m3 = m3;
    s.dalsi = NULL;
    return &s;
}

//vypíše senzor na obrazovku
void vypisSenzor (tSenzor *senzor){
    printf("Senzor id: %d, TimeStamp: %d, m3: %d", senzor->id, senzor->timestamp, senzor->m3);
}