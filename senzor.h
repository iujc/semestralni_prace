#include "datetime.h";

typedef struct senzor{
    int id;
    tDateTime timestamp;
    float m3;
    struct senzor* dalsi;
}tSenzor;

//a deklaraci funkcí:
tSenzor * vytvorSenzor(int id, tDateTime ts,float m3);
// vrací adresu dynamické alokace senzoru, ukazatel dalsi se nastaví na NULL
void vypisSenzor (tSenzor *senzor); //vypíše senzor na obrazovku