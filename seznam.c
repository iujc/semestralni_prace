#include "datetime.h"
#include "senzor.h"
#include <stdlib.h>
#include <string.h>


typedef struct {
    tDateTime od1;
    tDateTime do2;
    tSenzor *seznam;
}tSeznam;

tSeznam struktura;

//deklaraci funkcí:
int *dejSeznamSenzoru(){  // vrací pole id všech senzorů v souboru
    int sizeOfArray = 100, actualSize = 1;
    int* p_a = calloc(sizeOfArray, sizeof(int));
    if(struktura.seznam != NULL){
        *(p_a+0) = struktura.seznam -> id;
        tSenzor *_temp;
        _temp = struktura.seznam;
        while(_temp -> dalsi != NULL){
            actualSize++;
            if(actualSize == sizeOfArray){
                int* p_tempArray = calloc(sizeOfArray, sizeof(int));
                p_tempArray = memcpy(p_tempArray,p_a,sizeOfArray* sizeof(int));
                sizeOfArray = sizeOfArray + 10;
                p_a = calloc(sizeOfArray, sizeof(int));
                p_a = memcpy(p_a, p_tempArray, (sizeOfArray - 10) * sizeof(int));
                free(p_tempArray);
                //*(p_a+actualSize) = ; Ukládání
            }

        }
    }
}
tSeznam *nactiMereni(int id){
}
void vypisMereni(); // vypíše všechna měření, využívá vypisSenzor
tSenzor *odeberMereni(tDateTime timestamp); //odebere a vrátí požadované měření ze seznamu
void dealokujSeznam();// smaž (dealokuj) všechny záznamy a tSeznam
float **analyzuj(tDateTime od1, tDateTime do2);// Výpočet průměru pro všechny kombinace den v týdnu, který vrátí dynamické 2D pole 7x24 „den v týdnu“ x „hodina dne“ (v litrech)
void dealokujMatici(); // smaž (dealokuj) matici
float dejOdchylku(tDateTime timestamp);//vrátí odchylku v litrech daného záznamu (měření v dle timestamp) oproti správnému průměru z matice průměrů