#include "datetime.h"
#include "senzor.h"


typedef struct {
    tDateTime od1;
    tDateTime do2;
    tSenzor *seznam;
}tSeznam;



// vrací pole id všech senzorů v souboru
int *dejSeznamSenzoru();
tSeznam *nactiMereni(int id); // načte všechna měření daného senzoru, využívá vytvorSenzor
void vypisMereni(); // vypíše všechna měření, využívá vypisSenzor
tSenzor *odeberMereni(tDateTime timestamp); //odebere a vrátí požadované měření ze seznamu
void dealokujSeznam();// smaž (dealokuj) všechny záznamy a tSeznam
float **analyzuj(tDateTime od1, tDateTime do2);// Výpočet průměru pro všechny kombinace den v týdnu, který vrátí dynamické 2D pole 7x24 „den v týdnu“ x „hodina dne“ (v litrech)
void dealokujMatici(); // smaž (dealokuj) matici
float dejOdchylku(tDateTime timestamp);//vrátí odchylku v litrech daného záznamu (měření v dle timestamp) oproti správnému průměru z matice průměrů